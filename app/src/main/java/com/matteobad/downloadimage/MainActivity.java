package com.matteobad.downloadimage;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    DownloadImageTask downloadImageTask;
    ImageView imageView;
    String imageURL = "https://static.comicvine.com/uploads/square_small/11112/111122896/5402108-goku_v2_by_saodvd-dac3ion.png";

    public void download(View view) {
        try {
            Bitmap image = downloadImageTask.execute(imageURL).get();
            imageView.setImageBitmap(image);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        downloadImageTask = new DownloadImageTask();
        imageView = (ImageView) findViewById(R.id.imageView);
    }
}
